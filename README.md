# Trello Clone

This is a product as the outcome of the course about (pratical) Software Engineering.

Group members

- 1614150 Đặng Tuấn Vũ
- 1610391 Nguyễn Hoài Danh
- 1611017 Lê Thanh Hiếu
- 1611617 Nguyễn Anh Khoa
- 1613587 Vũ Khắc Tình

## The project

This project has two folders one for backend, one for frontend.

Backend and frontend project both has .env files to configure on run time. A few points to be taken in mind are:

- Backend port and frontend API port must be the same
- Backend mongo db connection string must be given and exists.

## Deployment

The app is currently hosted at [ttcnpm.luibo.online](http://ttcnpm.luibo.online). The front-end is built on production to a static file serve with dynamic loading using Javascript. The back-end is run on port 3000 and port-forward using nginx on the link [ttcnpm.api.luibo.online](http://ttcnpm.api.luibo.online). The database is hosted by a third-party.
